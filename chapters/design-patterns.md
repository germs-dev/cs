## Design patterns

{Describe the objective of design patterns}

### Categories

- Structural
- Behavioural
- Creational

### Common patterns

{Enumerate the common design patterns}

#### Factory pattern

{Describe the factory design pattern}

##### Example of factory pattern

Store a system configuration in an external text file and construct at runtime using the factory pattern.

#### Visitor pattern

{Describe the visitor design pattern}

#### Double dispatch

The visitor pattern exploits a feature of subtype polymorphism named "double dispatch."

{Describe double dispatch in subtype polymorphism}

##### Example of visitor pattern

Create a class hierarchy of shapes and define methods that operate on those shapes in a visitor class.

### Flyweight / singleton / memento

Hardware interface classes will typically be a `const` register map which is a flyweight pattern. This is also an example of a singleton pattern as it's only initialised once regardless of how many instances are created. Additionally if registers are actually wired to discrete lines they might be read-only, so the memento pattern is used to store the internal state.

## Reflection

"In computer science, reflection is the ability of a process to examine, introspect, and modify its own structure and behavior."

See [Wikipedia](https://en.wikipedia.org/wiki/Reflection_(computer_programming)).

## Dependency inversion

{What is dependency inversion?}

- [Refactoring Guru](https://refactoring.guru/design-patterns/)
- https://www.fluentcpp.com/2021/09/12/design-patterns-vs-design-principles-iterator-mediator-and-memento/
- [Extended examples](https://github.com/deanturpin/revision_design_patterns/)
- https://refactoring.guru/design-patterns/cpp

