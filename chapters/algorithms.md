## Algorithms

The Jonathan Boccara CppCon [105 STL Algorithms in Less Than an Hour](https://www.youtube.com/watch?v=2olsGf6JIkU) is well worth a watch for a quick overview.

### Sorting

Quicksort is the poster boy of sorting algorithms.

{Complexity of Quicksort}

Below is a Python implementation just for fun. But how is it implemented in C++?

```python
{Implementation of Quicksort in Python}
```

### std::sort uses Introsort

{Describe Introsort}

Introsort is in place but _not_ stable: i.e., equivalent elements are not guaranteed to remain in the same order. However, the Standard Library offers `stable_` versions of various sorting algorithms.

> If additional memory is available, `stable_sort` remains O(n ∗ logn). However, if it fails to allocate, it will degrade to an O(n ∗ logn ∗ logn) algorithm.

https://leanpub.com/cpp-algorithms-guide

#### Small array threshold

The threshold for switching to insertion sort varies for each compiler.

- Visual Studio 32
- GNU 16
- Clang is 30 for trivially constructible objects, otherwise 6

### `std::list`

`std::sort` requires random access to the elements, so `std::list` has its own
sort method, but it still (approximately) conforms to O(n log n). It can be
implemented with merge sort as moving elements is cheap with a linked list.

### Other sorting algorithms

Considerations for choosing an algorithm: size of input, Type of input: e.g., partially sorted, random.

{Compare the complexities of various sorting algorithms}

All of these are Θ(n log n) in all cases apart from Timsort has a Ω(n) and Quicksort has a terrible O(n^2) (if we happen to always pick the worst pivot). Quicksort is a good all rounder with O(n log n) average case. But it does have a O(n^2) worst case. It is said that this can be avoided by picking the pivot carefully but an example could be constructed where the chosen pivot is always the worst case.

- Mergesort breaks the problem into smallest units then combines adjacent
- Timsort finds runs of already sorted elements and then use mergesort; O(n) if already sorted
- Heapsort can be thought of as an improved selection sort
- Radix sort is a completely different solution to the problem
- A sorted array is already a min heap

## References

- [Comprehensive list of C++ standard library container complexities](https://users.cs.northwestern.edu/~riesbeck/programming/c++/stl-summary.html)
- [Interactive sorting animations](https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html)
- [Hacking Cpp -- a nice breakdown of Standard Library algorithms](https://hackingcpp.com/cpp/std/algorithms/finding.html)
- [Sorting Algorithms on Wikipedia](https://en.wikipedia.org/wiki/Sorting_algorithm)
- [Sorting Algorithms: Speed Is Found In The Minds of People - Andrei Alexandrescu - CppCon 2019](https://www.youtube.com/watch?v=FJJTYQYB1JQ).
- [Selection and insertion sort are actually the same thing](https://www.youtube.com/watch?v=pcJHkWwjNl4)
- [Analysis of algorithms set 3 asymptotic notations](https://www.geeksforgeeks.org/analysis-of-algorithms-set-3asymptotic-notations/)
- [The Intuitive Guide to Data Structures And Algorithms](https://www.interviewcake.com/data-structures-and-algorithms-guide)
- [Stony Brook Algorithm Repository](http://algorist.com/sections/Numerical_Problems.html)
