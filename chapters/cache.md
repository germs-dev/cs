## Caches

A cache is a small amount of unusually fast memory.

A modern multi-core machine will have a multi-level cache hierarchy, where the faster and smaller caches belong to individual processors. When one processor modifies a value in its cache, other processors cannot use the old value any more; and that memory location will be invalidated in all of the caches. Furthermore, since caches operate on the granularity of cache lines and not individual bytes, the entire cache line will be invalidated in all caches.

A multiprocessor is cache consistent if all writes to the same memory location are performed in some sequential order".

OpenAI:
{Describe processor caches}

- __Assume any pointer indirection is a cache miss__
- Smaller working data set is better
- Use as much of a cache entry as you can: don't read a whole cache line to check one bit
- Doing more work can be faster than doing less: sometimes more is more
- Predictable code
- Branchless code
- Sequential memory can be very fast due to prefetching
- Fewer evicted cache lines means more data in hot cache for the rest of the program
- Mis-predicted branches can evict cache entries (see [speculative execution](https://en.wikipedia.org/wiki/Speculative_execution))
- A dirty cache is not a problem for read-only data

### Cache consistency

> A multiprocessor is cache consistent if all writes to the same memory location are performed in some sequential order.

### Cache coherence

OpenAI:
{Describe cache coherence}

#### MESI cache coherence protocol

Each cache line has state, tag, data.

- Modified - write back the data before anybody else is allowed to read main memory
- Exclusive - mark if nobody is sharing, can change with impunity
- Shared
- Invalid - unused

#### Snooping

See [bus snooping](https://en.wikipedia.org/wiki/Bus_snooping).

- Write-invalidate (common)
- Write-update (causes greater bus activity)

#### Directory based coherence

Used for large CPUs. See [wiki](https://en.wikipedia.org/wiki Directory-based_cache_coherence).

#### Write-back versus write-through

All Intel-compatible CPUs during the last one/two decades used a write-back strategy for caches (which presumes fetching a cache line first to allow partial writes). Of course that's the theory, reality is slightly more complex than that.

### Cache misses

There are three kinds of cache misses:

1. Instruction read miss
1. Data read miss
1. Data write miss

OpenAI:
{Describe cache misses}

### Data cache

- Straight line code: no branches
- Linear array traversal

### Instruction cache

- Avoid iteration over heterogeneous sequences with virtual calls (sort first)
- Make "fast paths": branch-free sequences; integrate conditionals to avoid branches: `+ (vec.size() & 1)`
- Inline cautiously
- Take advantage of PGO and WPO

### Virtual functions

The way virtual functions work may cause issues with caching.  

1. Load vtable pointer: potentially data cache miss
1. Load virtual function pointer: potentially data cache miss
1. Then code of function is called: potential instruction cache miss

But we may be able to use [CRTP](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern) to avoid the first two.

#### CRTP

> The curiously recurring template pattern (CRTP) is an idiom in C++ in which a class X derives from a class template instantiation using X itself as template argument. More generally it is known as F-bound polymorphism, and it is a form of [static polymorphism](https://en.wikipedia.org/wiki/Static_dispatch).

```cpp
// Base class template using CRTP
template<typename Derived>
class Base {
public:
    void foo() {
        static_cast<Derived*>(this)->fooImpl();
    }
};

// Derived class using CRTP
class Derived : public Base<Derived> {
public:
    void fooImpl() {
        // Implementation of foo specific to Derived
        std::cout << "Hello from Derived!" << std::endl;
    }
};
```

### Cache related issues

- Memory banks
- Associativity
- Inclusive versus exclusive content
- Dirty cache not a problem for read-only
- Hyper-threading
- [False cache line sharing](https://en.wikipedia.org/wiki/False_sharing)... cache ping pong
- What tools are available for understanding cache use?

### Cache associativity

- Direct mapped cache
- N-way set associative cache
- Fully associative cache

### Cache oblivious algorithm design

Typically a [cache-oblivious algorithm](https://en.wikipedia.org/wiki/Cache-oblivious_algorithm) works by a recursive divide and conquer algorithm, where the problem is divided into smaller and smaller sub-problems. Eventually, one reaches a sub-problem size that fits into cache, regardless of the cache size.

- [Cache oblivious algorithm](https://en.wikipedia.org/wiki/Cache-oblivious_algorithm)
- [Loop nest optimisation](https://en.wikipedia.org/wiki/Loop_nest_optimization)

### Profiling

- PGO: profile guided optimisation
- WPO: whole program optimisation
- OProfile

### Data-oriented design

Design to get the most value out of every single cacheline that is read. Split apart the functions and data.

## Pipeline stall contributors

- Data hazards
- Control hazards: branches, exceptions
- Memory latency: cache misses

### Cache locality

{Describe cache locality}

### Further watching

- [What do you mean by "cache friendly"? -- Björn Fahller (code::dive 2019)](https://www.youtube.com/watch?v=Fzbotzi1gYs)
- [MIT pipelines](https://www.youtube.com/watch?v=lZ_7FeLgkSE&list=WL&index=140)
- [Trading at the speed of light](https://www.youtube.com/watch?v=8uAW5FQtcvE)
- [CPU Caches and Why You Care (1/2)](https://www.youtube.com/watch?v=WDIkqP4JbkE)
- [CPU Caches and Why You Care (2/2)](https://www.youtube.com/watch?v=WDIkqP4JbkE)

### TLB: Translation lookaside buffer

> The Translation Lookaside Buffer (TLB) is a hardware cache that is used in computer systems to improve the efficiency of virtual memory address translation. It is a small, high-speed cache that stores recently used virtual-to-physical address translations.

> When a program references a memory location, the virtual memory address needs to be translated into a physical memory address by the operating system. This translation process involves looking up a page table or a similar data structure. Since this translation operation is performed frequently, it can introduce overhead and impact system performance.

> The TLB acts as a cache for these translations, reducing the need for repeated lookups in the page table. It stores a subset of the most recently used translations, making it faster to access and retrieve the corresponding physical memory address.

> TLBs can be shared between multiple cores or processors, or each core may have its own dedicated TLB.

OpenAI:

{Describe translation lookaside buffer}

### Memory fences

> A memory fence (or barrier) is a programming language construct used to enforce an ordering constraint on memory operations issued by a thread of execution on a CPU. This typically means that operations issued prior to the fence are guaranteed to be performed before operations issued after the fence.

```cpp
const int num_mailboxes = 32;
std::atomic<int> mailbox_receiver[num_mailboxes];
std::string mailbox_data[num_mailboxes];
 
// The writer threads update non-atomic shared data 
// and then update mailbox_receiver[i] as follows:
mailbox_data[i] = ...;
std::atomic_store_explicit(&mailbox_receiver[i], receiver_id, std::memory_order_release);
 
// Reader thread needs to check all mailbox[i], but only needs to sync with one.
for (int i = 0; i < num_mailboxes; ++i)
    if (std::atomic_load_explicit(&mailbox_receiver[i],
        std::memory_order_relaxed) == my_id)
    {
        // synchronize with just one writer
        std::atomic_thread_fence(std::memory_order_acquire);
        // guaranteed to observe everything done in the writer thread
        // before the atomic_store_explicit()
        do_work(mailbox_data[i]);
    }
```

See [wiki](https://en.wikipedia.org/wiki/Memory_barrier) and [cppreference](https://en.cppreference.com/w/cpp/atomic/atomic_thread_fence).

### What pushes something out of the cache?

There are several factors that can push something out of the cache:

1. Cache size limitations: Caches have a limited capacity, and if the cache becomes full, the least recently used (LRU) items are typically pushed out to make space for new entries.
1. Cache eviction policies: Various cache eviction policies determine what gets pushed out when the cache is full. LRU (Least Recently Used) is one of the commonly used policies that evicts the oldest item that has not been accessed recently.
1. Time-based expiration: Some caches may have a time-based expiration mechanism. If an item remains unused for a certain period of time, it can be automatically removed from the cache to make room for other data.
1. Size-based eviction: In some cases, if the cache is designed to store items of a fixed size, new entries may replace older items when they exceed the cache's size limit.
1. Manual cache invalidation: Cache invalidation can be triggered manually by a programmer or system administrator. This is often done to ensure that the cache is up-to-date with the latest data, or to remove specific items from the cache.

### Further reading

- [NUMA](https://en.wikipedia.org/wiki/Non-uniform_memory_access)
- [False cache line sharing](https://en.wikipedia.org/wiki/False_sharing) -- cache ping pong
- [What Every Programmer Should Know About Memory](https://akkadia.org/drepper/cpumemory.pdf)
- [The Effect of Speculative Execution on Cache Performance](http://tnm.engin.umich.edu/wp-content/uploads/sites/353/2017/12/1994.04.The-Effect-Of-Speculative-Execution-On-Cache-Performance_Int_Parallel_Processing_Comp_Arch.pdf)
- <https://github.com/rollbear/cache-friendly-samples/blob/master/bheap_aux.cpp>
- [Crunching bytes](http://msinilo.pl/blog2/post/p425/)
- [Be nice to your cache](http://msinilo.pl/blog2/post/p614/)
- [Speculative execution](https://en.wikipedia.org/wiki/Speculative_execution)
- [MIT pipelines](https://www.youtube.com/watch?v=lZ_7FeLgkSE&list=WL&index=140)
- [Pipeline stall](https://en.wikipedia.org/wiki/Pipeline_stall)
- [Predication]( https://en.wikipedia.org/wiki/Predication_(computer\_architecture))
- [Vector processor](https://en.wikipedia.org/wiki/Vector_processor)
- [What every programmer should know about memory](https://people.freebsd.org/~lstewart/articles/cpumemory.pdf)
- [False sharing is not fun by Herb Setter](https://herbsutter.com/2009/05/15/effective-concurrency-eliminate-false-sharing/)
- [Gallery of processor cache effects by Igor Ostrovsky](http://igoro.com/archive/gallery-of-processor-cache-effects/)
- [CPU cache flushing fallacy](https://mechanical-sympathy.blogspot.com/2013/02/cpu-cache-flushing-fallacy.html)
- [Cache miss penalty on branching](https://stackoverflow.com/questions/22584961/cache-miss-penalty-on-branching)
- Optimising for instruction caches -- Amir Kleen
- Memory is not free (more on Vista performance) -- Sergey Solynik

