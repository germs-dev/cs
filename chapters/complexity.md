## Complexity

You should be comfortable explaining the complexity of your code. See the [Big O Cheatsheet](https://www.bigocheatsheet.com/).

{Markdown table of common complexities}

### Comparing two fundamental data structures

{Compare the average complexity of linked lists and arrays}

