## The rules of code

1. A single file should contain fewer than 20000 lines of code
1. It would be nice if a single routine could fit into 1000 lines of code (makes testing easier)
1. Ensure comments are ignored by prefixing "todo"
1. Use consistent formatting: even better, don't bother and let clang-format do it
1. Make things constant: it’s _much_ easier to reason about code when the data are immutable
1. Unit testing is awesome and will almost certainly catch on

### Unit testing

A test is _not_ a unit test if it:

- Talks to a database
- Communicates across the network
- Touches the file system
- Can't run at the same time as any of your other unit tests
- Does special things to your environment (such as editing config files) to run it

See the [complete article](https://www.artima.com/weblogs/viewpost.jsp?thread=126923).

