## The C++ Standard Library containers

{Why use the C++ Standard Library containers?}

{Enumerate the C++ Standard Library containers}

### Categories of containers

{Describe sequence versus associative containers}

### Choosing a container

- Number of elements
- Usage patterns: how often add/read/delete/traverse/rearrange?
- Where in the sequence will you add most often: end, beginning, middle?
- Do the elements need to be sorted? Do you even care if they're sorted?
- Do you need to iterate over the elements in a particular order?

