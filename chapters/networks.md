## Networking

### "Please Do Not Take Salami Pizza Away"

See [OSI on Wikipedia](https://en.wikipedia.org/wiki/OSI_model).

{Markdown table of the 7 OSI layers}

### Three way handshake

{Describe the TCP three way handshake}

SYN stands for "synchronise".

```bash
=> SYN
<= SYN-ACK
=> ACK
=> HTTP (request)
<= ACK
<= HTTP (response)
=> ACK
=> FIN
<= FIN-ACK
=> ACK
```
