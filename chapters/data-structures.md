## Data structures

It's important to know the common data structures and their characteristics.

{markdown list of the fundamental computer science data structures}

### `std::vector`

`std::vector` is the go-to container, so let's give it some special attention. It has contiguous storage -- therefore cache friendly -- and uses the [RAII](https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization) paradigm: the data are created on the heap and the allocation and deallocation (new/delete) are handled for you. Interestingly, `std::string` exhibits many of the same characteristics, but it's not quite containery enough to qualify.

#### How does my vector grow?

Estimate how many times the `fax` destructor is called below.

```cpp
#include <iostream>
#include <vector>

auto x = 0uz;

int main() {
    struct fax {

        // Constructor and destructor
        fax() { std::cout << x << " ctor\n"; }
        ~fax() { std::cout << "\t" << x << " dtor\n"; }

        // Copy constructors
        fax(const fax&) { std::cout << x << " copy ctor\n"; };
        fax(fax&&) { std::cout << x << " move ctor\n"; };

        // Assignment constructors
        fax& operator=(const fax&) {
            std::cout << x << " copy assignment ctor\n";
            return *this;
        }
        fax& operator=(fax&&) {
            std::cout << x << " move assignment ctor\n";
            return *this;
        };

        const size_t id = x++;
    };

    std::vector<fax> y;

    // Reduce copies by allocating up front
    // y.reserve(3);

    for (size_t i = 0; i < 3; ++i) {
        y.push_back(fax{});
        std::cout << "-- capacity " << y.capacity() << " --\n";
    }

    // fax f1 = fax{};
    // fax f2 = std::move(fax{});
}
}
```

See the program output (below), note how the capacity is growing exponentially (doubling each time).

```bash
1 ctor
2 move ctor
    2 dtor
-- capacity 1 --
3 ctor
4 move ctor
5 copy ctor
    5 dtor
    5 dtor
-- capacity 2 --
6 ctor
7 move ctor
8 copy ctor
9 copy ctor
    9 dtor
    9 dtor
    9 dtor
-- capacity 4 --
    9 dtor
    9 dtor
    9 dtor
```

For each push we call the default constructor to create a temporary object and then call the copy constructor to populate the new element with its data... so far so good. But crucially, when the vector is resized we must also copy _all_ of the existing elements into the new container. Not an issue for this simple case, but if the class is expensive to copy there could be trouble ahead; also, if there's a bug in the copy constructor, we may be corrupting existing _valid_ data simply by copying it around.

### Binary search trees

Binary search trees have an amortized complexity of O(log n) unless the tree is unbalanced. Worst case, if the tree is unbalanced, then it is O(n). `std::set` and `std::map` are implemented using a red-black tree, a type of balanced binary search tree. C++23 introduces `std::flat_map` which is implemented using contiguous storage to make it more cache-friendly.

{Describe binary search trees}

### Self-balancing binary search trees

A balanced tree is one of height O(log n), where n is the number of nodes in the tree. It is a sorted hierarchy of data where the left most node is the smallest, and the right most node is the largest.

- [Red Black tree](https://en.wikipedia.org/wiki/Red%E2%80%93black_tree)
- [B-Tree](https://en.wikipedia.org/wiki/B-tree)
- AVL tree
- [k-d tree](https://en.wikipedia.org/wiki/K-d_tree)

Below each node of a binary search tree is a mini tree. The top of the tree is the middle element.

```bash
e|            /e          
d|           d            
c|          / \c          
b|         b              
a|        / \ /a          
9|-----  /   9            
8|      /     \8          
7|     7                  
6|      \     /6          
5|-----  \   5            
4|        \ / \4          
3|         3              
2|          \ /2          
1|           1            
0|            \0          
```

### Hash tables

Hash tables have an amortized complexity of O(1) unless there are collisions. Worst case, if everything is in the same bin, then it is O(n). Additionally, if the proportion of slots -- known as "load factor" or "fill ratio" -- exceeds a threshold, then the hash table must be resized/rebuilt.

`std::unordered_set` and `std::unordered_map` are implemented using hash tables.

### Heaps

> A heap is a useful data structure when it is necessary to repeatedly remove the object with the highest (or lowest) priority.

Support of random access iterators is required to keep a heap structure internally at all times. A min heap is also a sorted list.

```bash
123456789abcedef
```

```bash
1
23
4567
89abcdef
```

Project down, the top of the tree is a smallest element.

```bash
       1
   2       3
 4   5   6   7
8 9 a b c d e f
```

```bash
       1
      / \
     /   \
    /     \
   2       3
  / \     / \
 4   5   6   7
/ \ / \ / \ / \
8 9 a b c d e f
```

### Comparison of heaps and binary search trees

{What is the difference between a heap and a binary search tree?}

### Singly linked lists

Adding/removing at the beginning is O(1), but adding at the end means search the whole list, therefore O(n). Searching is also O(n).

`std::forward_list` is a singly linked list.

### Doubly linked lists

Like a singly linked list but we can iterate both ways. It stores a pointer to both the beginning and end, therefore operations on the end are also O(1).

`std::list` is a doubly linked list.

### C++ Standard Library container complexities

{Markdown table of the C++ Standard Library containers and their complexities}

However, the conventional CS wisdom for when to use a linked list over contiguous storage hasn't applied for years: you have to measure. E.g., if a container fits in the cache, a vector might outperform everything else.

- [Time complexities for different data structures](https://www.geeksforgeeks.org/time-complexities-of-different-data-structures/)
- [Choosing the right container](https://medium.com/@rodrigues.b.nelson/choosing-wisely-c-containers-and-big-oh-complexity-64f9bd1e7e4c)
- Where does O(nlogn) comes from? [Stirling's approximation](https://en.wikipedia.org/wiki/Stirling%27s_approximation) is where
- [Data structures and algorithms](https://github.com/sachuverma/DataStructures-Algorithms)

