## Parallelism

Typically parallelism makes you think "threads within a process" but it's worth considering different points in the processing that could be executed/calculated in parallel.

### Kinds of parallelism

- Bit level
- Instruction level (ILP)
- Data (DLP/SIMD)
- Task parallelism (TLP/MIMD)

### The iron law of performance

See [wikipedia](https://en.wikipedia.org/wiki/Iron_law_of_processor_performance).

```bash
time/program = instructions/program * clockCycles/instruction * time/clockCycles
```

### Amdahl's law

{Describe Amdahl's law}

It's very tempting to optimise the parts your are familiar with, but it's not obvious how much your efforts will benefit the overall process. _Spend your time on the bottlenecks._

- [YouTube/MIT - parallel processing](https://www.youtube.com/watch?v=aZqPLM0wrlY)
- [SSE and AVX](https://www.codingame.com/playgrounds/283/sse-avx-vectorization/what-is-sse-and-avx)
- [Moore's law hits the roof](https://www.agner.org/optimize/blog/read.php?i=417)
- [Accelerated Massive Parallelism](https://en.wikipedia.org/wiki/C%2B%2B_AMP)
- [Pluralsight - High-performance Computing in C++](https://app.pluralsight.com/library/courses/cpp-high-performance-computing/table-of-contents)

