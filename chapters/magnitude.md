## The magnitude of it all

In 2014 [Randall Munroe](https://www.youtube.com/watch?v=I64CQp6z0Pk) estimated that Google stores 10 exabytes of data across all of its operations. However, as a C++ developer, you will only come across at most petabytes of _storage_; and if CPUs are topping out at gigahertz, then you won't see anything much _faster_ than a nanosecond.

```bash
                        1 000  kilo | milli .001
                    1 000 000  mega | micro .000 001
                1 000 000 000  giga | nano  .000 000 001
            1 000 000 000 000  tera | pico  .000 000 000 001
        1 000 000 000 000 000  peta | femto .000 000 000 000 001
    1 000 000 000 000 000 000   exa | atto  .000 000 000 000 000 001
1 000 000 000 000 000 000 000 zetta | zepto .000 000 000 000 000 000 001
```

See [list of SI prefixes](https://en.wikipedia.org/wiki/Metric_prefix#List_of_SI_prefixes).

You may be tempted to use [binary prefixes](https://en.wikipedia.org/wiki/Binary_prefix) to be more precise -- kibi, mebi, gibi, etc -- but most people won't know what you're talking about. Also, manufacturers tend to use 1000<sup>3</sup> rather than 2<sup>20</sup> because it makes their performance look better.

### Important speeds you should be aware of

See [why is everyone in such a rush?](https://norvig.com/21-days.html)

```bash
1/1000000000 second == 1 nanosecond 
```

Approximate durations of typical operations (rounded to help remember.)

| Action | Duration (nanoseconds) |
| --- | --- |
| L1 cache read, variable increment | <1 |
| Branch misprediction (how do you measure this?) | 5 |
| L2 cache read, `std::atomic` | 10 |
| `std::mutex/scoped_lock` | 20 |
| Fetch from main memory | 100 |
| Semaphore acquire | 200 |
| Send 2KiB over 1Gbps network | 20,000 |
| Create a `std::thread` | 20,000 |
| Send packet from US to Europe and back | 200,000,000 |
