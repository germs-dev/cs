## Template metaprogramming
__
"Generic programming is why we can't have nice things" -- [Andrei Alexandrescu](https://en.wikipedia.org/wiki/Andrei_Alexandrescu)
__

- Move computations from run-time to compile-time, with run-time performance benefits
- `constexpr` -- find undefined behaviour at compile time
- The template rabbit hole goes deep!

{Describe template metaprogramming}

{Describe the use of templates in the C++ Standard Library}

### SFINAE

{Describe SFINAE}

