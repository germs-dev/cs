## Binary structure

See [Linux Debuginfo Formats - DWARF, ELF, dwo, dwp - What are They All? - Greg Law - CppCon 2022](https://www.youtube.com/watch?v=l3h7F9za_pc).

| Section | Description |
| ------- | ----------- |
| stack \/ | Local variables |
| heap /\ | Dynamic memory |
| `.rodata` | Read-only data |
| `.bss` | Uninitialised data |
| `.data` | Initialised data |
| `.text` | Code |

{Describe the segments found in a binary executable}

