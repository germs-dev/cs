## C++ function parameters

For simple readonly types, pass by const value.

```cpp
void func(const int p) {
}
```

For large readonly types -- i.e., larger than a pointer -- pass by const reference. You should also consider if the type is trivially copyable: requiring a shallow or deep copy.

```cpp
void func(const big_type& p) {
}
```

If the first thing you do is make a copy, pass by non-const value.

```cpp
void func(int p) {
}
```

If you want to modify the _caller's_ copy, pass by non-const reference.

```cpp
void func(int& p) {
}
```

### class or struct?

The only difference between a class and a struct is the default access level: public, protected, private.

But typically a struct is used for "plain old data" or memory-mapped data where you don't want the members to align in a predictable way (virtuals/inheritance adds the complication of a vtable pointer.)

Either way, you can test your assertions at compile time with `static_assert` and the [type traits header](https://en.cppreference.com/w/cpp/types/has_virtual_destructor).

```cpp
struct A {
  // public: <-- default for structs
  int x_;
};
```

If the data are constrained by an invariant -- i.e., not all values are valid -- use a class.

```cpp
class B {
  private: // <-- default for classes
  int x_; // must be between 0-1000
};
```
