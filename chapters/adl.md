## Argument dependent lookup (aka Koenig lookup)

> Argument-dependent lookup, also known as ADL, or Koenig lookup, is the
> set of rules for looking up the unqualified function names in function-call
> expressions, including implicit function calls to overloaded operators. These
> function names are looked up in the namespaces of their arguments in addition
> to the scopes and namespaces considered by the usual unqualified name lookup.

Notice begin and end parameters don't specify a namespace, see [ADL](https://en.cppreference.com/w/cpp/language/adl).

```cpp
#include <iostream>
#include <vector>
#include <iterator>

int main() {

    const std::vector v{1, 2, 3, 4, 5};
    std::copy(cbegin(v), cend(v), std::ostream_iterator<int>(std::cout, "n"));
}
```
