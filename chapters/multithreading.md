## Multithreading

Multithreaded concepts are important: e.g., atomics, locks, issues with different designs, how to make things thread safe.

Why use multithreading? For performance and/or separation of concerns.

### Reentrancy

A routine can be described as reentrant if it can be interrupted and not leave the operation partially complete: __the invariants remain true__.

### Invariants

A good way to reason about your code is to consider the invariants that must be true at any given point in time. For a linked list, node A's next pointer must point to node B and node B's previous pointer must point to node A. However, if you are deleting an element from a linked list there will be a period where this invariant does not hold, and you must acquire mutual access to both nodes whilst the list is being updated; otherwise another thread may see the list in an invalid state.

OpenAI:
{describe invariants}

### Idempotence

A function is idempotent if it can be applied multiple times without changing the outcome. E.g., repeatedly pressing the "on" switch of an "on/off" pair: it's still on. See [Wikipedia](https://en.wikipedia.org/wiki/Idempotence).

### Race conditions

__tl;dr__ "Race condition: an unfortunate order of execution causes undesirable behaviour."

OpenAI:
{describe race conditions in computer science}

### Considerations

- Ensure critical sections are as small as possible
- Only a problem for multiple writers (multiple readers OK)
- Too few threads: algorithm is sub-optimal
- Too many threads: overhead of creating/managing and partitioning the data is greater than processing advantage; software threads outnumber the available hardware threads and the OS must intervene
- Data races, deadlocks and livelocks -- unsynchronised access to shared memory can introduce race conditions and undefined behaviour (program results depend non-deterministically on the relative timings of two or more threads)
- Threads versus processes
- STO: strict timestamp ordering (younger transactions wait for older transactions)

### Deadlocks and livelocks

See the [Dining philosophers problem](https://en.wikipedia.org/wiki/Dining_philosophers_problem).

A real world example: Ken and Barbie walk over to the barbecue. Ken grabs the last remaining sausage and attempts to acquire the hot sauce; meanwhile, Barbie has already secured the hot sauce and is waiting for the sausage to become available. A sausage cannot be enjoyed without hot sauce, so neither can proceed and remain stuck in a deadlock indefinitely.

A livelock occurs when measures are taken to avoid a deadlock, but the measures themselves do not resolve the conflict. For example, Ken and Barbie both agree to put down their respective items and try again; however, they both pick up the same item agains and the deadlock continues.

OpenAI:
{Compare deadlocks and livelocks}

### Avoiding deadlocks

Run your code on different numbers of CPUs; it's interesting how the bottlenecks move around depending on the available resources.

OpenAI:
{How to avoid deadlocks}

#### See also

- [Chandy–Misra–Haas algorithm resource model](https://en.wikipedia.org/wiki/Chandy%E2%80%93Misra%E2%80%93Haas_algorithm_resource_model)
- [Wait-for graph](https://en.wikipedia.org/wiki/Wait-for_graph)
- [Banker's algorithm](https://en.wikipedia.org/wiki/Banker%27s_algorithm)

#### Additionally

- Try to avoid calling out to external code while holding a lock
- If you ever need to acquire two locks at once, document the ordering thoroughly and make sure you always use the same order -- or use `std::scoped_lock`
- Immutability is great for multithreading: immutable means thread safety (functional programming works well concurrently partly due to the emphasis on immutability)

### Synchronising threads

OpenAI:
{Compare mutex and semaphore}

- A mutex is a locking mechanism used to synchronize access to a resource
- A semaphore is signalling mechanism: "I am done, you can carry on"

See [Mutex vs Semaphore](https://www.geeksforgeeks.org/mutex-vs-semaphore/).

This is getting into the weeds, but mutexes and the data they protect can happily be decalared consecutively in the code; so consider what this might mean when both mutex and data are in the same cache line.

### Threads versus processes

__tl;dr__ A thread is a branch of execution. A process can consist of multiple threads.

OpenAI:
{Compare threads and processes}

### Thread bests practice

- Don't try to go lock-free unless you really have to: locks are expensive, but rarely the bottleneck
- Document the thread-safety of your types: most types don't need to be thread-safe, they just need to not be thread hostile; i.e., "You can use them from multiple threads, but it's your responsibility for taking out locks if you want to share them
- Don't access the UI (except in documented thread-safe ways) from a non-UI thread
- Use locks when you access mutable shared data, both for reads and writes

### References

- [Thread safety](https://en.wikipedia.org/wiki/Thread_safety)
- [Memory barrier](https://en.wikipedia.org/wiki/Memory_barrier)
- [The Amazing Performance of C++17 Parallel Algorithms, is it Possible?](https://www.bfilipek.com/2018/11/parallel-alg-perf.html)
- [C++ Concurrency in Action](https://www.manning.com/books/c-plus-plus-concurrency-in-action-second-edition)

