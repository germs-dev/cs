## Hash functions

{What is a hash function?}

### Properties of a hashing algorithm

- Stable: the same input generates the same output every time
- Uniform: the hash values should be uniformly distributed through the available space
- Efficient: the cost of generating a hash must be balanced with application need
- Secure: the cost of finding data that produces a given hash is prohibitive

### String hashing

- Sum ASCII values: not uniform, not secure
- Fold bytes of every four characters into an integer: not secure
- CRC32: not secure
- MD5: not efficient, not secure
- SHA2: stable, uniform, not efficient, secure

