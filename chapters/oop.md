## Object-oriented programming

{Describe object-oriented programming}

- Encapsulation: tie the data and the functions that are acting together in a class.
- Composition
- [Delegation](https://en.wikipedia.org/wiki/Delegation_(computing)) is calling a routine "in another context", for instance calling a base class member
function from an instance of a derived class.
- Open recursion
- Abstraction: providing only essential information to the outside world and hiding their background details, separation of interface and implementation.
- Decoupling: removing dependencies on external computations/data.
- Low coupling, high cohesion

### Differences between a class and a struct

In C++, the only difference between a `class` and a `struct` is the default access level. In a `class`, members are private by default, while in a `struct`, members are public by default. Also, the default inheritance access level is different: `class` uses private inheritance by default, while `struct` uses public inheritance by default.

### Polymorphism

{Describe the types of polymorphism}

### Virtual destructors

A standard question at interviews! A very nuanced feature of OOP in C++ but you can now test this at compile time with type traits.

Destructors must be declared virtual when the class contains virtual functions or is part of a polymorphic hierarchy. This ensures that the destructor of a derived class is called if it is deleted via a pointer to the polymorphic base class.

### Virtual function specifier

> The virtual specifier specifies that a non-static member function is virtual and supports dynamic dispatch. It may only appear in the decl-specifier-seq of the initial declaration of a non-static member function (i.e., when it is declared in the class definition).

In derived classes, you should mark overridden methods as `override` to express intention and also protect from the base class being changed unwittingly.

### Virtual tables

A non-virtual class has a size of 1 because in C++ classes can’t have zero size (objects declared consecutively can't have the same address). A virtual class has a size of 8 on a 64-bit machine because there’s a hidden pointer inside it pointing to a vtable. vtables are static translation tables, created for each virtual-class.

### RAII

{Describe RAII}

### Overloading

{Describe overload resolution strategies}
