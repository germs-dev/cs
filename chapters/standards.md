## C++ standards

The big things for me are ranges and views, so effortlessly express and manipulate ranges of data with zero iterators; they read naturally and are also lazy-evaluated (and gcc 14 brings `ranges::to<>` to the party). Concepts I've used a little, but mostly to narrow the allowed types of an auto parameter: e.g., `std::floating_point auto`.

I've attempted to deep dive into templates a few times and it's really quite an unpleasant language! However, whilst not a recent addition, `constexpr` is becoming increasingly supported; it gives you the readability of "normal" code with the potential performance benefits of compile-time. And it also finds UB! However, you quickly hit the issue where something isn't `constexpr` so you have to either implement it yourself or not bother.

Coroutines are quite high-profile but I haven't got into them yet. Finally, the multidimensional array operator seemed like a game changer but I've only used it once.

You'll have to reach into gcc 14 but `std::print` is very welcome; as are modules (which might actually be available in 13.2 with `-fmodules-fs` flag) but poorly supported and not quite what I'd like yet.

Notable mentions from C++17 are structured bindings and execution policy.

### C++26

- `constexpr` cmath
- Placeholder variables: "_"

### C++23

See the [presentation](https://www.youtube.com/watch?v=b0NkuoUkv0M) by Marc Gregoire (CppCon 2022).

You've gotta track gcc head to avoid disappointment.

- Multidimensional array operator: `data[x, y, z]` and `std::mdspan`
- `uz` literals
- `.contains` for strings and containers
- Extended `constexpr` for `std::optional` and `std::variant`
- `[[assume(true)]]` -- introduce UB into your code with assumptions
- `std::print` and `std::println` -- gcc 14
- New container adapters: `std::flat_map` and `std::flat_set` (contiguous binary search trees) -- not in gcc 14
- `import <iostream>;` -- modules, gcc 13 with flag `-fmodules-ts`
- Deducing this -- simplify a lot of const ref overloads
- `consteval` -- immediate functions: only execute at compile time
- A standard coroutine generator defined in `<generator>`
- `<stack_trace>`
- `std::expected`
- `std::byteswap`

### Ranges and views (part two)

A lot of effort has gone into ranges and views in C++23.

#### std::ranges

- `starts_with`
- `shift_left`
- `ranges::to<>` -- gcc14, convert a range to a vector (for instance)
- `find_if`
- `contains`
- `contains_subrange`
- `std::ranges::fold_left` -- gcc 13.1

#### std::views

- `zip`
- `adjacent`
- `pairwise`
- `chunk`
- `slide`
- `chunk_by`
- `stride` -- take every nth element
- `join/join_with` -- I've had no luck with these outside of a trivial example
- `repeat`
- `iota` -- infinite views may be more performant as no boundary check
- `slide` -- love these
- `enumerate` --  gcc 13.2, easily enumerate your range-based for loops, you get a pair of index and the element

### C++20

- Ranges and views -- part one
- `.contains` for maps
- `.starts_with` for strings
- `std::jthread` -- thread you don't have to explicitly join (it's the same as `std::thread` but joins in the destructor), also has a built-in stop token
- coroutines
- modules -- in the standard but slow support
- concepts -- templates becoming easier each time
- `std::barrier` for thread synchronisation (like `std::latch` but reusable)

### C++17

- Structured bindings -- hard to go back when you've used them
- Execution policy -- slow to be supported but an interesting option for parallelising existing code
- `std::filesystem` -- from Boost
- `std::string_view`
- `std::clamp`
- `[[maybe_unused]]`
- `<string_view>`
- `std::byte`
- Class Template Argument Deduction (CTAD) enables class template deduction from the constructor call

Also see [cppreference](https://en.cppreference.com/w/cpp/17).

### C++14

C++14 is an extension and improvement of C++11.

- Binary literals: `0b1111000`
- Return type deduction: `auto` return type
- Generic lambda expressions: `auto` in lambda parameters
- Variable templates: `template <class T> constexpr T bins = T{24'576};`
- `decltype(auto)`

### C++11

- Range-based for loops
- Brace initialisers
- `constexpr`

