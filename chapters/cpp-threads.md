## Creating threads in C++

This is all a lot easier since `std::thread` was introduced in C++11. Now we have a platform dependent interface. See the [C++ concurrency support library examples](https://threads.cpp.run/).

However, remember there is an overhead in creating a thread: if the operation you're repeating is quick then could it actually take longer to parallelise it? You need to profile: how long does it take to just create a thread?

### Ways to create a thread

#### `std::async`

Think of it like pushing a calculation into the background. `std::async` executes a function asynchronously and returns a `std::future` that will eventually hold the result of that function call. It's quite a nice way to reference the result of a calculation executed in another thread. Also, it manages the issue of creating too many threads: they will just be executed sequentially. Additionally, exceptions thrown in the asynchronous routine destroy the future and the exception is rethrown by the calling `get()` routine. You ought to pass it a strategy: `std::launch::async` or `std::launch::deferred` as a first parameter.

#### `std::thread`

You need to call `join()` for every thread created with `std::thread`. This can be done by storing your threads as a vector and joining each of them in a loop.

#### `std::jthread`

Like a regular thread but you don't have to join it in the caller: it actually joins for you in the destructor. Quite nice to not have to remember to join, but joining all your threads can be a convenient synchronisation point.

#### Execution policies

Many of the Standard Library algorithms can take an execution policy, which is quite an exciting way to parallelise existing code. But remember it offers no protection of shared data: it's still just a bunch of threads. 

- `std::execution::seq`: execution may not be parallelized
- `std::execution::par`: execution may be parallelized
- `std::execution::par_unseq`: execution may be parallelized, vectorized, or migrated across threads (such as by a parent-stealing scheduler)
- `std::execution::unseq`: execution may be vectorized, e.g., executed on a single thread using instructions that operate on multiple data items

##### C++ algorithms that can take an execution policy

Some of these have an `_if` version that takes a additional predicate: e.g., `std::replace` and `std::replace_if`.

1. `std::sort`
1. `std::copy`
1. `std::transform`
1. `std::accumulate`
1. `std::for_each`
1. `std::reduce`
1. `std::inclusive_scan`
1. `std::exclusive_scan`
1. `std::transform_reduce`
1. `std::remove`
1. `std::count`
1. `std::max_element`
1. `std::min_element`
1. `std::find`
1. `std::generate`

### Acquiring resources

#### `std::mutex`

A standard way to protect access to something, but there are multiple ways to unlock it.

#### Acquiring multiple resources

Here be dragons! In the shape of deadlocks. There are several strategies to improve your chances of not become stuck, see the deadlock chapter for more. You can use `std::scoped_lock` with multiple resources or single, but also I think it expresses intention better, by virtue of having "scope" in the name.

- [Concurrency in C++: A Programmer’s Overview (part 1 of 2) - Fedor Pikus - CppNow 2022](https://www.youtube.com/watch?v=ywJ4cq67-uc)

### Standard library threading types

- `std::mutex`
- `std::atomic` - an operation that is guaranteed to operate as a single transaction
- `std::scoped_lock`
- `std::lock_guard`
- `std::thread` and `std::join`
- `std::jthread` (auto join)

See the [Standard Library concurrency support library](https://en.cppreference.com/w/cpp/thread).

### Let's ask OpenAI to write a producer-consumer solution in C++

```cpp
{Write a producer-consumer solution in C++}
```

