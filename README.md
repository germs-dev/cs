---
title: Teach yourself C++ in 45 years
subtitle: A Computer Science survival guide for C++/Linux developers
author: Dean Turpin
---

## Forward

### An ode to C++

Let's start with a haiku written by a computer.

```
{Write a haiku about C++}
```

This reference is a distillation of [15+ years of online logbook notes](https://germs-dev.gitlab.io/deanturpin/) into only the essentials that have continued to remain relevant as a senior software developer today. Just for fun -- and where the topic is readily available or established -- I have [reached out to OpenAI](https://turpin.cloud/) to provide a paragraph or two. Consequently, the exact content and the chapter order will vary [each night](https://gitlab.com/germs-dev/cs/-/pipeline_schedules). Hopefully this will keep repeat visits interesting and also prevents me focusing all my attention on the first few chapters.

If time is tight, try the [random daily chapter](daily.html). And you can also [raise a ticket](https://gitlab.com/germs-dev/cs/-/issues/new) on this repo.

<!-- Chapters are appended here by the build pipe, see the chapters/ dir -->

